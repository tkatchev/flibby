
all: flibby

flibby: flibby.cc archive.h  db.h  deps.h  fb2.h  funcs.h posix.h cairo.cc
	$(CXX) -std=c++17 -O3 flibby.cc cairo.cc -larchive `pkg-config --cflags --libs libxml-2.0 xapian-core pangocairo` -o flibby


