# Introduction #

`flibby` is a command-line, text-only utility for managing, full-text indexing and searching libraries of ebooks in fb2 format.

Reading and converting fb2 files to PDF is also supported.

# Compiling and installing #

You will need `libxml2`, `libarchive`,`xapian` and a modern version of `gcc`. For converting to PDF you'll also need `pango` and `cairo`. (If you're on a Linux system then these are likely already installed.) 

Type `make` to build. Put the resulting binary (`flibby`) anywhere in your path. It uses no external resources and writes no config file.

# Usage #

### Indexing ###

    $ flibby index <indexdir> <files>...

or

    $ flibby index-meta <indexdir> <files>...

This command will index the ebooks in `<files>` and place the full-text index into the directory `<indexdir>`. (The directory will be created if it does not exist.)

Fb2 files and fb2 files in archives (zip, tar.gz, rar, etc.) are supported. The program will autodetect archive type automatically. Several archives or files can be listed.

Indexed files are tracked using a full-text hash; indexing the same document more than once will not put duplicate entries into the index.

If `index-meta` is used, then only the metadata will be indexed; ebook text will be parsed and hashed, but won't be searchable. This drastically reduces the disk space and processing time needed for indexing.

'Metadata' is book title, author, genre and language.

### Searching ###

    $ flibby search <indexdir> <query>

or

    $ flibby export <indexdir> <query>

or
    $ flibby go <indexdir> <query>

These command will search the index for matching ebooks.

`<indexdir>` is the directory will the full-text index, created by `flibby index`.

`<query>` is a search query, in Xapian syntax. (See also: http://xapian.org/docs/queryparser.html)

* One or more terms separated by whitespace.
* Use `and`, `or`, `not` boolean operators to chain terms together. (Terms separated by whitespace act like `and`.)
* Use `adj` and `near` operators to search for terms adjacent to each other in the text. (`adj` preserves term order while `near` does not.)
* Use parenthesis for grouping terms.
* Prefix each term with a field specification:
    * `author:` search in author names
    * `title:` search in book title
    * `heading:` search in chapter and section headings 
    * `text:` search in book paragraph text
    * `poem:` search in book verse text
    * `label:` search in ornamental and epigraph labels
    * `lang:` match book language code

Terms without a prefix will search in any type of field.

Examples of search queries:

    author:morrison and (text:detroit or text:michigan)

    detroit adj michigan

    sword heading:unicorn

Calling `search` will output book title, author and archive location (filename of archive and filename within archive) on the screen.

Calling `export` will extract the book contents as files and place them into a specified directory location, filed according to book author and book title.

Calling `go` combines `search` and `read`. (See below.) The first search result will be output in a human-readable way to the screen. Use `--offset` to choose which entry will be output.

#### These commands also accept options: ####

`--lang <language>` is the (human) language to use for interpreting words in the query; for example `en` for English, `es` for Spanish, `ru` for Russian. This language is used for stemming search query terms. By default a language-neutral query parser is used.

`--offset <n>` starts output from the Nth search result.

`--limit <n>` outputs N search results. Use `-` to output everything.

`--output <directory>` sets the root directory for writing exported files. The current directory is used by default.

### Extracting ###

    $ flibby dump <archive> [entry]

This command will output the raw fb2 contents of an ebook. The first argument is an archive (zip, tar.gz, rar, etc.), the second argument is a name of a file in the archive.

If the first argument is not an archive, then the command simply outputs the given file. (Like `cat`.)

If the first argument is an archive with a single file in it, then the file will be extracted and output. 

If the first argument is an archive with several files, then the second argument must match a filename in the archive. That file will be extracted and output.

Example:

    $ flibby dump mybooks.zip book1.fb2

### Reading ###

    $ flibby read <archive> [entry]

Same as `flibby dump`, except that instead of dumping the raw fb2 data, the data is parsed and a human-readable ebook text is output.

This serves as a primitive text-mode fb2 reader.

(Note: no attempt to preserve formatting is made, only headings and structure is preserved properly.)

If the command is run interactively, then the output will be automatically sent to `less`, with primitive formatting. (Bold and italic for highlighting headings.)

Example:

    $ flibby read mybooks.zip book1.fb2
    $ flibby read book2.fb2

### Converting ###

    $ flibby convert <files>...

Reads all the fb2 files in all the archives provided on the command line, converts them to PDF and files the result according to author and title. (Using the same procedure that `flibby export` does.)

This is meant to be used after running `flibby export`, but works as a standalone process too.

Available options:

`--output <directory>` sets the root directory for writing converted files. The current directory is used by default.

`--font <font-string>` sets the font to be used. Default is "Serif 14".

`--margin <margin>` sets the margin, in typographic points. Default is 6.

`--size <paper-size` sets the page size. Must be one of "A0" to "A9". Default is A5.

Example:

    $ flibby convert */*/*.fb2.zip --font "Noto Sans 12" --size "A4"




