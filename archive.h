#pragma once


size_t archive_num_entries(const std::string& zipfile) {

    size_t ret = 0;

    struct archive* a;
    struct archive_entry* entry;

    a = archive_read_new();
    archive_read_support_format_all(a);
    archive_read_support_format_raw(a);

    int r;

    r = archive_read_open_filename(a, zipfile.c_str(), 10240);

    if (r < 0) 
        return ret;

    while (1) {
        r = archive_read_next_header(a, &entry);

        if (r == ARCHIVE_EOF)
            return ret;

        if (r != ARCHIVE_OK)
            return ret;

        ret++;
        archive_read_data_skip(a);
    }        

    archive_read_close(a);
    archive_read_free(a);

    return ret;
}

enum {
    READ_ZIP_DUMP  = 1,
    READ_ZIP_PARSE = 2,
    READ_ZIP_STOP  = 4
};

template <typename FUNC1, typename FUNC2, typename FUNC3>
void read_zip(const std::string& zipfile, bool need_ne, FUNC1 check, FUNC2 process1, FUNC3 process2) {

    size_t num_entries = 0;

    if (need_ne) {
        num_entries = archive_num_entries(zipfile);
    }
    
    struct archive* a;
    struct archive_entry* entry;

    a = archive_read_new();
    archive_read_support_format_all(a);
    archive_read_support_format_raw(a);

    int r;

    r = archive_read_open_filename(a, zipfile.c_str(), 10240);

    if (r < 0) {
        throw std::runtime_error(std::string("Could not open archive: ") + archive_error_string(a));
    }

    while (1) {
        r = archive_read_next_header(a, &entry);

        if (r == ARCHIVE_EOF)
            break;

        if (r != ARCHIVE_OK)
            throw std::runtime_error(std::string("Failed reading archive: ") + archive_error_string(a));

        std::string filename = archive_entry_pathname(entry);

        int what = check(filename);

        char buff[64*1024];
        size_t size = 64*1024;

        if (!(what & (READ_ZIP_DUMP | READ_ZIP_PARSE))) {

            archive_read_data_skip(a);

        } else if (what & READ_ZIP_DUMP) {

            while (1) {
                r = archive_read_data(a, &buff, size);

                if (r == 0)
                    break;

                if (r < 0) {
                    throw std::runtime_error(std::string("Failed reading data: ") + archive_error_string(a));
                }

                process1(std::string(buff, r));
            }

        } else if (what & READ_ZIP_PARSE) {

            xmlSAXHandler saxHandler;
            xmlParserCtxtPtr ctxt;

            memset(&saxHandler, 0, sizeof(saxHandler));

            saxHandler.initialized = XML_SAX2_MAGIC;

            saxHandler.startElementNs = &FB2Parser::startElementNs;
            saxHandler.endElementNs   = &FB2Parser::endElementNs;
            saxHandler.characters     = &FB2Parser::characters;
            saxHandler.warning        = &FB2Parser::warning;
            saxHandler.error          = &FB2Parser::error;
            saxHandler.fatalError     = &FB2Parser::fatalError;

            FB2Parser parser;

            ctxt = xmlCreatePushParserCtxt(&saxHandler, &parser, buff, 0, filename.c_str());

            while (1) {
                r = archive_read_data(a, &buff, size);

                if (r == 0)
                    break;

                if (r < 0) {
                    throw std::runtime_error(std::string("Failed reading data: ") + archive_error_string(a));
                }

                r = xmlParseChunk(ctxt, buff, r, 0);

                if (r != 0) {
                    //xmlErrorPtr err = xmlGetLastError();
                    //std::cout << "Bad XML: " << err->message << std::endl;
                    parser.ok = false;
                }

                if (!parser.ok)
                    break;
            }

            if (parser.ok) {
                xmlParseChunk(ctxt, buff, 0, 1);
            }

            xmlFreeParserCtxt(ctxt);

            if (!parser.ok)
                continue;

            if (parser.book.text.empty())
                continue;

            parser.book.archive = zipfile;

            if (num_entries > 1) {
                parser.book.filename = filename;
            }

            process2(parser.book);
        }

        if (what & READ_ZIP_STOP)
            break;
    }

    archive_read_close(a);
    archive_read_free(a);
}

// This is how the moronic libarchive API moronically calls mkdir().

void archive_mkdir (const std::string& path) {
    std::string err("Making directory failed: ");

    struct archive *a;
    struct archive_entry *entry;

    a = archive_write_disk_new();

    entry = archive_entry_new();
    archive_entry_set_pathname(entry, path.c_str());
    archive_entry_set_filetype(entry, AE_IFDIR);
    archive_entry_set_perm(entry, 0777);

    if (archive_write_header(a, entry) != ARCHIVE_OK) {
        throw std::runtime_error(err + archive_error_string(a));
    }

    if (archive_write_finish_entry(a) != ARCHIVE_OK) {
        throw std::runtime_error(err + archive_error_string(a));
    }

    archive_entry_free(entry);

    if (archive_write_free(a) != ARCHIVE_OK) {
        throw std::runtime_error(err + archive_error_string(a));
    }
}

template <typename FUNC>
void write_zip(const std::string& path, const std::string& filename, const std::string& extension, FUNC func) {
    std::string err("Zipping file failed: ");

    std::string outname = path + "/" + filename + extension;

    struct archive *a;
    struct archive_entry *entry;

    a = archive_write_new();

    if (archive_write_set_format_zip(a) != ARCHIVE_OK) {
        throw std::runtime_error(err + archive_error_string(a));
    }
        
    if (archive_write_open_filename(a, outname.c_str()) != ARCHIVE_OK) {
        throw std::runtime_error(err + archive_error_string(a));
    }

    for (size_t n = 0; ; ++n) {
        auto [ filename, data ] = func(n);

        if (filename.empty()) {
            break;
        }

        entry = archive_entry_new();
        archive_entry_set_pathname(entry, filename.c_str());
        archive_entry_set_size(entry, data.size());
        archive_entry_set_filetype(entry, AE_IFREG);
        archive_entry_set_perm(entry, 0644);
        archive_entry_set_mtime(entry, time(NULL), 0);

        if (archive_write_header(a, entry) != ARCHIVE_OK) {
            throw std::runtime_error(err + archive_error_string(a));
        }

        if (archive_write_data(a, data.c_str(), data.size()) != data.size()) {
            throw std::runtime_error(err + archive_error_string(a));
        }

        archive_entry_free(entry);
    }

    if (archive_write_close(a) != ARCHIVE_OK) {
        throw std::runtime_error(err + archive_error_string(a));
    }

    if (archive_write_free(a) != ARCHIVE_OK) {
        throw std::runtime_error(err + archive_error_string(a));
    }
}


void write_zip(const std::string& path, const std::string& filename, const std::string& data) {

    write_zip(path, filename, ".zip",
              [&](size_t n) {
                  if (n == 0) {
                      return std::make_pair(filename, data);
                  }

                  return std::make_pair(std::string(), std::string());
              });
}

