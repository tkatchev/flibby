
#include <cairo.h>
#include <cairo-pdf.h>
#include <pango/pangocairo.h>

#include <string>
#include <iostream>
#include <vector>
#include <tuple>


void render_pdf(const std::string& title, const std::string& author, const std::string& text, const std::string& font,
                double PAGEW, double PAGEH, double MARGIN, double spacing, const std::string& filename) {

    cairo_surface_t* surface = cairo_pdf_surface_create(filename.c_str(), PAGEW, PAGEH);

    cairo_pdf_surface_set_metadata(surface, CAIRO_PDF_METADATA_TITLE, title.c_str());
    cairo_pdf_surface_set_metadata(surface, CAIRO_PDF_METADATA_AUTHOR, author.c_str());

    cairo_t* cr = cairo_create(surface);

    cairo_set_source_rgb(cr, 0, 0, 0);

    PangoLayout* layout = pango_cairo_create_layout(cr);
    PangoFontDescription* font_description = pango_font_description_from_string(font.c_str());

    pango_layout_set_font_description(layout, font_description);

    pango_layout_set_wrap(layout, PANGO_WRAP_WORD_CHAR);
    pango_layout_set_justify(layout, TRUE);
    pango_layout_set_width(layout, (PAGEW - MARGIN * 4) * PANGO_SCALE);
    pango_layout_set_alignment(layout, PANGO_ALIGN_LEFT);
    pango_layout_set_single_paragraph_mode(layout, TRUE);
    pango_layout_set_indent(layout, MARGIN * 4 * PANGO_SCALE);

    std::string para;
    double y = -1;

    auto go =
        [&]() {

            if (para.empty()) {
                return;
            }

            pango_layout_set_text(layout, para.c_str() + 1, -1);

            bool bold = (para[0] == 'B');
            bool italic = (para[0] == 'I');

            if (bold || italic) {
                PangoAttrList* list = pango_attr_list_new();

                if (bold) {
                    PangoAttribute* attr = pango_attr_weight_new(PANGO_WEIGHT_BOLD);
                    pango_attr_list_insert(list, attr);
                }

                if (italic) {
                    PangoAttribute* attr = pango_attr_style_new(PANGO_STYLE_ITALIC);
                    pango_attr_list_insert(list, attr);
                }

                pango_layout_set_attributes(layout, list);
                pango_attr_list_unref(list);

            } else {
                pango_layout_set_attributes(layout, NULL);
            }

            std::vector<std::tuple<gint, gint, double, bool>> sublayouts;

            for (GSList* l = pango_layout_get_lines_readonly(layout); l; l = l->next) {

                PangoLayoutLine* line = reinterpret_cast<PangoLayoutLine*>(l->data);

                PangoRectangle size;
                pango_layout_line_get_extents(line, NULL, &size);

                double lh = spacing * (double)size.height / PANGO_SCALE;

                if (y < 0) {
                    y = MARGIN;
                }

                if (y + lh >= PAGEH) {
                    y = MARGIN;
                    sublayouts.emplace_back(line->start_index, line->length, y, true);

                } else if (sublayouts.empty()) {
                    sublayouts.emplace_back(line->start_index, line->length, y, false);

                } else {
                    std::get<1>(sublayouts.back()) += line->length;
                }

                y += lh;
            }

            bool first = true;

            for (const auto& [ start, length, _y, pagebreak ] : sublayouts) {

                if (pagebreak) {
                    cairo_show_page(cr);
                }

                cairo_move_to(cr, MARGIN * 2, _y);

                if (sublayouts.size() == 1) {
                    pango_cairo_show_layout(cr, layout);

                } else {
                    PangoLayout* xlayout = pango_layout_copy(layout);

                    if (!first) {
                        pango_layout_set_indent(xlayout, 0);
                    } else {
                        first = false;
                    }

                    pango_layout_set_text(xlayout, pango_layout_get_text(layout) + start, length);

                    pango_cairo_show_layout(cr, xlayout);
                    g_object_unref(xlayout);
                }
            }

            para.clear();
        };

    for (char c : text) {

        if (c == '\n') {
            go();
        } else {
            para += c;
        }
    }

    go();

    g_object_unref(layout);
    pango_font_description_free(font_description);
    cairo_surface_destroy(surface);
    cairo_destroy(cr);
}
