#pragma once


struct Database {

    enum {
        DOCVAL_ARCHIVE = 0,
        DOCVAL_FILENAME = 1,
        DOCVAL_AUTHOR = 2,
        DOCVAL_TITLE = 3
    };

    Xapian::WritableDatabase db;
    size_t n;
    bool index_metadata_only;

    Database(const std::string& dbpath, bool imo = false) : db(dbpath, Xapian::DB_CREATE_OR_OPEN), n(0), index_metadata_only(imo) {}

    void _process(Xapian::TermGenerator& tg, const std::string& s, const char* pref) {

        tg.index_text(s, 1, pref);
        //tg.index_text(s);
        //tg.increase_termpos();
    }

    static std::string get_author(Book& book) {

        auto trim =
            [](const std::string& s) {
                auto a = s.find_first_not_of(' ');
                auto b = s.find_last_not_of(' ');

                if (a == std::string::npos) {
                    return std::string();
                }

                return s.substr(a, 1 + b - a);
            };

        std::string ret;

        for (const auto& i : book.author) {

            if (i.empty()) {
                continue;
            }

            if (ret.size() > 0)
                ret += ",";

            for (const auto& j : i) {

                if (j.size() == 0)
                    continue;

                if (ret.size() > 0)
                    ret += " ";

                ret += trim(j);
            }
        }

        return ret;
    }

    std::string tolower(const std::string& s) {
        std::string ret;

        for (char c : s) {
            ret += ::tolower(c);
        }

        return ret;
    }

    void process(Book& book) {

        Xapian::TermGenerator tg;

        std::string lang = tolower(book.lang);

        try {
            tg.set_stemmer(Xapian::Stem(lang));
        } catch (Xapian::Error& e) {
            //
        }

        Xapian::Document doc;
        tg.set_document(doc);

        _process(tg, book.title, "S");

        for (const auto& i : book.author) {
            for (const auto& j : i) {
                _process(tg, j, "A");
            }
        }

        _process(tg, lang, "L");
        _process(tg, book.genre, "G");

        size_t hash = 0;
        
        for (const auto& i : book.text) {
            hash += std::hash<std::string>()(i.second);

            if (index_metadata_only)
                continue;

            switch (i.first) {

            case Book::PLAIN:
                _process(tg, i.second, "XP");
                break;

            case Book::TITLE:
                _process(tg, i.second, "XT");
                break;

            case Book::POEM:
                _process(tg, i.second, "XV");
                break;

            case Book::LABEL:
                _process(tg, i.second, "XL");
                break;

            case Book::BR:
                break;
            }
        }

        std::string id = book.archive;

        if (book.filename.size() > 0) {
            id += "#" + book.filename;
        }

        doc.add_value(DOCVAL_ARCHIVE, book.archive);
        doc.add_value(DOCVAL_FILENAME, book.filename);
        doc.add_value(DOCVAL_AUTHOR, get_author(book));
        doc.add_value(DOCVAL_TITLE, book.title);

        std::string hid = std::to_string(hash);

        doc.add_term(hid);
        db.replace_document(hid, doc);

        ++n;
        if ((n % 100) == 0) {
            db.commit();
        }

        std::cerr << n << "    " << id << std::endl;
    }
};

struct SearchParams {
    std::string stemmer;
    size_t limit;
    size_t offset;
    std::string out_dir;
};

template <typename FUNC>
void search(const std::string& dbpath, const SearchParams& params, const std::string& query, FUNC f) {

    Xapian::Stem stem;

    try {
        stem = Xapian::Stem(params.stemmer);
    } catch (Xapian::Error& e) {
        //
    }
        
    Xapian::QueryParser qp;
    qp.set_stemmer(stem);
    qp.set_stemming_strategy(qp.STEM_SOME);
    qp.set_default_op(Xapian::Query::OP_AND);
    qp.add_prefix("title", "S");
    qp.add_prefix("author", "A");
    qp.add_prefix("lang", "L");
    qp.add_prefix("genre", "G");
    qp.add_prefix("text", "XP");
    qp.add_prefix("heading", "XT");
    qp.add_prefix("poem", "XV");
    qp.add_prefix("label", "XL");

    qp.add_prefix("", "XP");
    qp.add_prefix("", "S");
    qp.add_prefix("", "A");
    qp.add_prefix("", "XT");
    qp.add_prefix("", "XV");
    qp.add_prefix("", "XL");

    Xapian::Query q = qp.parse_query(query,
                                     Xapian::QueryParser::FLAG_DEFAULT |
                                     Xapian::QueryParser::FLAG_PURE_NOT |
                                     Xapian::QueryParser::FLAG_BOOLEAN_ANY_CASE);

    Xapian::Database db(dbpath);

    Xapian::Enquire enquire(db);
    enquire.set_query(q);
    enquire.set_docid_order(Xapian::Enquire::ASCENDING);

    size_t offset = params.offset;
    size_t pagesize = params.limit;
        
    Xapian::MSet mset = enquire.get_mset(offset, pagesize);

    size_t total = mset.size() + offset;
    size_t n = offset + 1;

    for (Xapian::MSetIterator m = mset.begin(); m != mset.end(); ++m, ++n) {

        Xapian::docid did = *m;

        Xapian::Document d = m.get_document();

        f(d, n, total, params);
    }
}

void list_terms(const std::string& dbpath, const std::string& prefix) {

    Xapian::Database db(dbpath);

    auto b = db.allterms_begin(prefix);
    auto e = db.allterms_end(prefix);

    while (b != e) {
        std::cout << (*b).substr(prefix.size()) << " " << b.get_termfreq() << std::endl;
        ++b;
    }
}

