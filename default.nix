{
  pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/refs/tags/22.05.tar.gz") {}
} :
with pkgs;
stdenv.mkDerivation {
  name = "flibby";
  version = "2022.06";
  src = nix-gitignore.gitignoreSourcePure [ ./.gitignore ] ./.;
  nativeBuildInputs = [ pkg-config ];
  buildInputs = [ xapian cairo.dev libxml2 pango libarchive ];
  installPhase = ''
    mkdir -p $out/bin
    cp flibby $out/bin
  '';
}

