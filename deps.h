#pragma once

#include <stdio.h>
#include <memory.h>

#include <xapian.h>

#include <libxml/xmlmemory.h>
#include <libxml/parser.h>

#include <archive.h>
#include <archive_entry.h>

#include <limits>
#include <string>
#include <set>
#include <vector>
#include <iostream>

