#pragma once


struct Book {
    std::string archive;
    std::string filename;
    std::string title;
    std::vector< std::vector<std::string> > author;
    std::string lang;
    std::string genre;

    enum tag_t {
        PLAIN,
        TITLE,
        POEM,
        LABEL,
        BR
    };

    std::vector< std::pair<tag_t,std::string> > text;
};

struct FB2Parser {

    Book book;

    bool in_title_info;
    bool in_author;
    bool in_first_name;
    bool in_middle_name;
    bool in_last_name;
    bool in_book_title;
    bool in_lang;
    bool in_genre;
    bool in_body;
    bool in_title;
    bool in_subtitle;
    bool in_p;
    bool in_v;
    bool in_text_author;
    bool in_a;

    bool ok;

    FB2Parser() : 
        in_title_info(false),
        in_author(false),
        in_first_name(false),
        in_middle_name(false),
        in_last_name(false),
        in_book_title(false),
        in_lang(false),
        in_genre(false),
        in_body(false),
        in_title(false),
        in_subtitle(false),
        in_p(false),
        in_v(false),
        in_text_author(false),
        in_a(false),
        ok(true)
        {}


    static void startElementNs(void* ctx, 
                              const xmlChar* localname, 
                              const xmlChar* prefix, 
                              const xmlChar* URI, 
                              int nb_namespaces, 
                              const xmlChar** namespaces, 
                              int nb_attributes, 
                              int nb_defaulted, 
                              const xmlChar** attributes) {

        FB2Parser& p = *(static_cast<FB2Parser*>(ctx));

        std::string name((char*)localname);

        if (name == "title-info") {
            p.in_title_info = true;

        } else if (name == "author") {
            p.in_author = true;

            if (p.in_title_info) {
                p.book.author.push_back(std::vector<std::string>(3));
            }

        } else if (name == "first-name") {
            p.in_first_name = true;

        } else if (name == "middle-name") {
            p.in_middle_name = true;

        } else if (name == "last-name") {
            p.in_last_name = true;

        } else if (name == "book-title") {
            p.in_book_title = true;

        } else if (name == "lang") {
            p.book.lang.clear();
            p.in_lang = true;

        } else if (name == "genre") {
            p.book.genre.clear();
            p.in_genre = true;

        } else if (name == "body") {
            p.in_body = true;

        } else if (name == "title") {
            p.in_title = true;

        } else if (name == "subtitle") {
            p.in_subtitle = true;

            p.book.text.push_back(std::make_pair(Book::LABEL, ""));

        } else if (name == "p") {
            p.in_p = true;

            p.book.text.push_back(std::make_pair(p.in_title ? Book::TITLE : Book::PLAIN, ""));

        } else if (name == "v") {
            p.in_v = true;

            p.book.text.push_back(std::make_pair(Book::POEM, ""));

        } else if (name == "text-author") {
            p.in_text_author = true;

            p.book.text.push_back(std::make_pair(Book::LABEL, ""));

        } else if (name == "a") {
            p.in_a = true;
        }

    }

    static void endElementNs(void* ctx, const xmlChar* localname, const xmlChar* prefix, const xmlChar* URI) {

        FB2Parser& p = *(static_cast<FB2Parser*>(ctx));

        std::string name((char*)localname);

        if (name == "title-info") {
            p.in_title_info = false;

        } else if (name == "author") {
            p.in_author = false;

        } else if (name == "first-name") {
            p.in_first_name = false;

        } else if (name == "middle-name") {
            p.in_middle_name = false;

        } else if (name == "last-name") {
            p.in_last_name = false;

        } else if (name == "book-title") {
            p.in_book_title = false;

        } else if (name == "lang") {
            p.in_lang = false;

        } else if (name == "genre") {
            p.in_genre = false;

        } else if (name == "body") {
            p.in_body = false;

        } else if (name == "title") {
            p.in_title = false;

        } else if (name == "subtitle") {
            p.in_subtitle = false;

        } else if (name == "p") {
            p.in_p = false;

        } else if (name == "v") {
            p.in_v = false;

        } else if (name == "stanza") {
            p.book.text.push_back(std::make_pair(Book::BR, "stanza"));

        } else if (name == "text-author") {
            p.in_text_author = false;

        } else if (name == "a") {
            p.in_a = false;
        }
    }

    static void characters(void *ctx, const xmlChar* ch, int len) {

        FB2Parser& p = *(static_cast<FB2Parser*>(ctx));

        std::string chars((char*)ch, len);

        if (p.in_title_info) {

            if (p.in_author) {

                if (p.in_first_name) {
                    p.book.author.back()[0] += chars;

                } else if (p.in_middle_name) {
                    p.book.author.back()[1] += chars;

                } else if (p.in_last_name) {
                    p.book.author.back()[2] += chars;
                }

            } else if (p.in_book_title) {
                p.book.title += chars;

            } else if (p.in_lang) {
                p.book.lang += chars;

            } else if (p.in_genre) {
                p.book.genre += chars;
            }

        } else if (p.in_body) {

            // !p.in_a && 
            if (p.in_p || p.in_v || p.in_text_author || p.in_subtitle) {

                p.book.text.back().second += chars;
            }

        }
    }

    static void fatalError(void* ctx, const char* msg, ...) {

        FB2Parser& fsm = *(static_cast<FB2Parser*>(ctx));
        va_list args;
        va_start(args, msg);
        vprintf( msg, args );
        va_end(args);
    }

    static void error(void* ctx, const char* msg, ...) {

        FB2Parser& fsm = *(static_cast<FB2Parser*>(ctx));
        /*
        va_list args;
        va_start(args, msg);
        vprintf( msg, args );
        va_end(args);
        */

        fsm.ok = false;
    }

    static void warning(void* ctx, const char* msg, ...) {

        /*
        FB2Parser& fsm = *(static_cast<FB2Parser*>(ctx));
        va_list args;
        va_start(args, msg);
        vprintf( msg, args );
        va_end(args);
        */
    }
};

