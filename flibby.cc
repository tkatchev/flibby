#include "deps.h"
#include "fb2.h"
#include "db.h"
#include "archive.h"
#include "funcs.h"
#include "posix.h"

struct Arguments {
    SearchParams params = { "", 10, 0, "" };
    bool pretty = true;
    PDFParams pdf;
};

Arguments parse_options(std::vector<std::string>& args) {

    auto get_no_arg =
        [](size_t i, std::vector<std::string>& args) {
            args.erase(args.begin() + i, args.begin() + i + 1);
        };

    auto get_arg =
        [](size_t i, std::vector<std::string>& args) {
            if (i + 1 >= args.size()) {
                throw std::runtime_error("Argument error: '" + args[i] + "' option requires an argument.");
            }

            std::string ret(args[i + 1]);
            args.erase(args.begin() + i, args.begin() + i + 2);
            return ret;
        };

    Arguments _ret;
    auto& [ ret, pretty, pdf ] = _ret;

    for (size_t i = 1; i < args.size(); ) {

        if (args[i] == "-l" || args[i] == "--lang") {
            ret.stemmer = get_arg(i, args);

        } else if (args[i] == "-o" || args[i] == "--offset") {
            ret.offset = std::stoul(get_arg(i, args));

        } else if (args[i] == "-n" || args[i] == "--limit") {
            std::string offset = get_arg(i, args);

            if (offset == "-") {
                ret.limit = std::numeric_limits<size_t>::max();
            } else {
                ret.limit = std::stoul(offset);
            }

        } else if (args[i] == "-x" || args[i] == "--output") {
            ret.out_dir = get_arg(i, args) + "/";

        } else if (args[i] == "-p" || args[i] == "--plain") {
            get_no_arg(i, args);
            pretty = false;

        } else if (args[i] == "-f" || args[i] == "--font") {
            pdf.font = get_arg(i, args);

        } else if (args[i] == "-m" || args[i] == "--margin") {
            pdf.margin = std::stoul(get_arg(i, args));

        } else if (args[i] == "-s" || args[i] == "--size") {
            std::string arg = get_arg(i, args);

            if (arg.size() != 2 || arg[0] != 'A' || arg[1] < '0' || arg[1] > '9') {
                throw std::runtime_error("Page size must be one of 'A0' to 'A9'.");
            }

            double w = 33.11 * 72;
            double h = 46.81 * 72;
            unsigned int size = arg[1] - '0';

            for (unsigned int s = 0; s < size; ++s) {
                double tmp = h;
                h = w;
                w = tmp / 2;
            }

            pdf.width = w;
            pdf.height = h;

        } else {
            ++i;
        }
    }

    return _ret;
}

void usage(const std::string& argv0) {

    std::cerr << "Usage: " << std::endl
              << "       " << argv0 << " index       {index-database-dir} {ebook-archive}..." << std::endl
              << "       " << argv0 << " index-meta  {index-database-dir} {ebook-archive}..." << std::endl
              << "       " << argv0 << " search      {index-database-dir} [options] {query}..." << std::endl
              << "       " << argv0 << " export      {index-database-dir} [options] {query}..." << std::endl
              << "       " << argv0 << " dump        {ebook-or-archive} [archive-entry]" << std::endl
              << "       " << argv0 << " read        {ebook-or-archive} [archive-entry]" << std::endl
              << "       " << argv0 << " go          {index-database-dir} [options] {query}..." << std::endl
              << "       " << argv0 << " convert     [options] {ebook-or-archive}..." << std::endl
              << "       " << argv0 << " list-langs  {index-database-dir}" << std::endl
              << "       " << argv0 << " list-genres {index-database-dir}" << std::endl << std::endl
              << " Options for searching and exporting:" << std::endl
              << "       -l, --lang {language-code} : set morphology rules for processing words in search query." << std::endl
              << "       -o, --offset {n}           : start with the Nth result in output." << std::endl
              << "       -n, --limit {n}            : output N results; '-' means output everything." << std::endl
              << " Options for exporting and converting:" << std::endl
              << "       -x, --output {directory}   : directory where to write exported files." << std::endl
              << " Options for reading:" << std::endl
              << "       -p, --plain                : don't output bold and italic." << std::endl
              << " Options for converting:" << std::endl
              << "       -f, --font {font-string}   : set PDF font. (Default is 'Serif 14'.)" << std::endl
              << "       -m, --margin {margin}      : set PDF page margins, in points. (Default is 6.)" << std::endl
              << "       -s, --size A[0-9]          : set PDF page size. One of 'A0' to 'A9'. (Default is 'A5'.)" << std::endl
        ;
}

int main(int argc, const char** argv) {

    //LIBXML_TEST_VERSION

    try {

        std::vector<std::string> args(argv, argv + argc);

        if (args.size() < 3) {
            usage(args[0]);
            return 1;
        }

        auto _p = parse_options(args);

        auto& params = _p.params;
        auto& pretty = _p.pretty;
        auto& pdf = _p.pdf;

        std::string mode(args[1]);
        
        if (mode == "index" || mode == "index-meta") {

            std::string dbpath(args[2]);

            Database db(dbpath, (mode == "index-meta"));
            
            for (size_t i = 3; i < args.size(); ++i) {

                std::string zipfile(args[i]);

                read_zip(zipfile, true,
                         [](const std::string& fname) { return READ_ZIP_PARSE; },
                         [](const std::string&) {},
                         [&db](Book& book) { db.process(book); });
            }

        } else if (mode == "search" || mode == "export") {

            if (args.size() < 4) {
                usage(args[0]);
                return 1;
            }

            std::string dbpath(args[2]);
            std::string query;

            for (size_t i = 3; i < args.size(); ++i) {
                query += " ";
                query += args[i];
            }

            search(dbpath, params, query, (mode == "search" ? serp_entry : export_entry));

        } else if (mode == "list-genres" || mode == "list-langs") {

            if (args.size() < 3) {
                usage(args[0]);
                return 1;
            }

            std::string dbpath(args[2]);

            list_terms(dbpath, (mode == "list-genres" ? "G" : "L"));

        } else if (mode == "dump") {

            std::string zipfile(args[2]);
            std::string entry;

            if (args.size() >= 4) 
                entry = args[3];

            read_zip(zipfile, false,
                     [&entry](const std::string& fname) { return (entry.empty() || entry == fname ? (READ_ZIP_DUMP | READ_ZIP_STOP) : 0); },
                     [](const std::string& data) { std::cout << data; },
                     [](Book& book) {});

        } else if (mode == "read") {

            std::string zipfile(args[2]);
            std::string entry;

            if (args.size() >= 4) 
                entry = args[3];

            read_zip(zipfile, false,
                     [&entry](const std::string& fname) { return (entry.empty() || entry == fname ? (READ_ZIP_PARSE | READ_ZIP_STOP) : 0); },
                     [](const std::string&) {},
                     [&pretty](Book& book) {

                         auto read = [&](bool human) { print_book(book, human && pretty); };

                         humanistic_output(read);
                     });

        } else if (mode == "go") {

            if (args.size() < 4) {
                usage(args[0]);
                return 1;
            }

            std::string dbpath(args[2]);
            std::string query;

            for (size_t i = 3; i < args.size(); ++i) {
                query += " ";
                query += args[i];
            }

            std::string zipfile;
            std::string entry;

            params.limit = 1;

            search(dbpath, params, query,
                   [&zipfile, &entry](const Xapian::Document& d, size_t n, size_t total, const SearchParams& params) {
            
                       zipfile = d.get_value(Database::DOCVAL_ARCHIVE);

                       if (d.get_value(Database::DOCVAL_FILENAME).size() > 0) {
                           entry = d.get_value(Database::DOCVAL_FILENAME);
                       }
                   });

            ///

            if (zipfile.empty()) {
                return 1;
            }

            read_zip(zipfile, false,
                     [&entry](const std::string& fname) { return (entry.empty() || entry == fname ? (READ_ZIP_PARSE | READ_ZIP_STOP) : 0); },
                     [](const std::string&) {},
                     [&pretty](Book& book) {

                         auto read = [&](bool human) { print_book(book, human && pretty); };

                         humanistic_output(read);
                     });

        } else if (mode == "convert") {

            for (size_t ii = 2; ii < args.size(); ++ii) {

                std::string zipfile(args[ii]);

                read_zip(zipfile, false,
                         [](const std::string& fname) { return READ_ZIP_PARSE; },
                         [](const std::string&) {},
                         [&pdf, &params](Book& book) {

                             std::cout << convert_book_to_pdf(book, pdf, params.out_dir) << std::endl;
                         });
            }

        } else {

            usage(args[0]);
            return 1;
        }

    } catch (std::exception& e) {
        std::cerr << "ERROR: " << e.what() << std::endl;
        return 1;

    } catch (Xapian::Error& e) {
        std::cerr << "ERROR: " << e.get_msg() << std::endl;
        return 1;
        
    } catch (...) {
        std::cerr << "UNKNOWN ERROR" << std::endl;
        return 1;
    }

    return 0;
}

