#pragma once

std::string utf8_first_letter(const std::string& s) {
    if (s.empty()) return s;

    unsigned char f = s[0];
    size_t n = 0;

    if ((f >> 7) == 0) {
        n = 1;

    } else if ((f >> 5) == 6) {
        n = 2;

    } else if ((f >> 4) == 14) {
        n = 3;

    } else if ((f >> 3) == 30) {
        n = 4;
    }

    return s.substr(0, n);
}

void serp_entry(const Xapian::Document& d, size_t n, size_t total, const SearchParams& params) {
            
    std::cout << std::endl << d.get_value(Database::DOCVAL_AUTHOR) << ": " << d.get_value(Database::DOCVAL_TITLE) << std::endl;

    std::cout << "\t" << d.get_value(Database::DOCVAL_ARCHIVE);

    if (d.get_value(Database::DOCVAL_FILENAME).size() > 0)
        std::cout << "\t" << d.get_value(Database::DOCVAL_FILENAME);

    std::cout << std::endl;
}

std::pair<std::string, std::string> get_document_location(std::string title, std::string author, const std::string& extension,
                                                          const std::string& out_dir) {
    {
        auto x = author.find(',');
        author = author.substr(0, x);
        auto y = author.rfind(' ');

        if (y != std::string::npos) {
            author = author.substr(y + 1) + " " + author.substr(0, y);
        }
    }

    if (author.empty()) {
        author = "_";
    }
    
    std::string path = out_dir + utf8_first_letter(author);

    path += '/';
    path += author;

    std::string filename;

    for (unsigned char c : title) {
        if (c == '/') {
            filename += ' ';
        } else {
            filename += c;
        }
    }

    filename += extension;

    if (filename.size() > 254) {
        size_t prev = 0;
        for (size_t i = 0; i <= 254; ++i) {
            if (filename[i] == ' ') {
                prev = i;
            }
        }

        filename = filename.substr(0, prev);
        filename += extension;
    }

    return std::make_pair(path, filename);
}

std::pair<std::string, std::string> get_document_location(const Xapian::Document& doc, const std::string& extension, const SearchParams& params) {

    std::string title = doc.get_value(Database::DOCVAL_TITLE);
    std::string author = doc.get_value(Database::DOCVAL_AUTHOR);

    return get_document_location(title, author, extension, params.out_dir);
}

void export_entry(const Xapian::Document& doc, size_t n, size_t total, const SearchParams& params) {

    auto write =
        [&](const std::string& path, const std::string& filename, const std::string& data) {
            archive_mkdir(path);
            write_zip(path, filename, data);
        };

    auto [ path, filename ] = get_document_location(doc, ".fb2", params);

    std::string zipfile = doc.get_value(Database::DOCVAL_ARCHIVE);
    std::string entry = doc.get_value(Database::DOCVAL_FILENAME);
    std::string data;

    read_zip(zipfile, false,
             [&entry](const std::string& fname) { return (entry.empty() || entry == fname ? (READ_ZIP_DUMP | READ_ZIP_STOP) : 0); },
             [&](const std::string& _data) { data += _data; },
             [](Book& book) {});

    std::cout << "Exporting " << n << "/" << total << ", " << path << "/" << filename << ".zip" << std::endl;

    write(path, filename, data);
}

std::string linewrap(const std::string& s) {

    std::string ret;
    size_t n = 0;

    for (unsigned char c : s) {

        if (c == ' ' && n >= 70) {
            ret += '\n';
            n = 0;

        } else if (c == '\n') {
            ret += c;
            n = 0;

        } else {
            ret += c;

            // Handle UTF8.
            // If the eighth and seventh bits are '10', then this byte is part of a UTF8 multi-byte symbol.

            if ((c >> 6) != 2)
                ++n;
        }
    }

    return ret;
}

std::string _SGR(char c = '0') {
    char ret[16] = "\e[0m";
    ret[2] = c;
    return ret;
}

void print_book(Book& book, bool ansi = true) {

    auto SGR = [&](char c = '0') { return ansi ? _SGR(c) : ""; };

    std::cout << std::endl << SGR('1') << book.title << SGR() << std::endl;

    std::cout << std::endl << SGR('4') << Database::get_author(book) << SGR() << std::endl;

    std::cout << std::endl << "-----------------------------------------" << std::endl << std::endl;
    
    for (const auto& i : book.text) {

        if (i.second.empty())
            continue;
        
        switch (i.first) {

        case Book::PLAIN:
            std::cout << " " << linewrap(i.second) << std::endl << std::endl;
            break;

        case Book::TITLE:
            std::cout << "* " << SGR('1') << i.second << SGR() << " *" << std::endl << std::endl;
            break;

        case Book::POEM:
            std::cout << "|  " << SGR('3') << i.second << SGR() << std::endl;
            break;

        case Book::LABEL:
            std::cout << "   " << SGR('3') << i.second << SGR() << std::endl << std::endl;
            break;

        case Book::BR:
            std::cout << std::endl;
            break;
        }
    }
}

extern void render_pdf(const std::string& title, const std::string& author, const std::string& text, const std::string& font,
                       double PAGEW, double PAGEH, double MARGIN, double spacing, const std::string& filename);

struct PDFParams {
    std::string font = "Serif 14";
    double width = 417.6;
    double height = 597.6;
    double margin = 6;
    double spacing = 1;
};

std::string convert_book_to_pdf(Book& book, const PDFParams& params, const std::string& out_dir) {

    std::string author = Database::get_author(book);

    auto [ path, filename ] = get_document_location(book.title, author, ".pdf", out_dir);

    archive_mkdir(path);

    std::string text;

    for (const auto& i : book.text) {

        if (i.second.empty())
            continue;
        
        switch (i.first) {

        case Book::PLAIN:
            text += ' ';
            text += i.second;
            break;

        case Book::TITLE:
            text += 'B';
            text += i.second;
            break;

        case Book::POEM:
            text += "I| ";
            text += i.second;
            break;

        case Book::LABEL:
            text += 'I';
            text += i.second;
            break;

        case Book::BR:
            text += ' ';
            break;
        }

        text += "\n";
    }

    std::string ret = path + '/' + filename;

    render_pdf(book.title, author, text, params.font, params.width, params.height, params.margin, params.spacing, ret);

    return ret;
}

