#pragma once

#if defined (__unix__) || (defined (__APPLE__) && defined (__MACH__))

#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

void __sigchld(int signum) {
    ::wait(NULL);
    ::_exit(0);
}

template <typename FUNC>
void humanistic_output(FUNC f) {

    bool human = ::isatty(1);

    if (!human) {
        f(human);
        return;
    }

    int pfd[2];

    if (::pipe(pfd) < 0) {
        throw std::runtime_error("Could not pipe()");
    }

    if (::signal(SIGCHLD, __sigchld) == SIG_ERR) {
        throw std::runtime_error("Could not signal()");
    }

    pid_t pid = ::fork();

    if (pid < 0) {
        throw std::runtime_error("Could not fork()");
    }

    if (pid == 0) {
        if (::dup2(pfd[0], 0) < 0) {
            throw std::runtime_error("Could not dup2()");
        }

        if (::close(pfd[0]) < 0) {
            throw std::runtime_error("Could not close()");
        }

        if (::close(pfd[1]) < 0) {
            throw std::runtime_error("Could not close()");
        }

        if (::execlp("less", "less", "-r", NULL) < 0) {
            throw std::runtime_error("Could not execlp(\"less -r\")");
        }

    } else {
        if (::dup2(pfd[1], 1) < 0) {
            throw std::runtime_error("Could not dup2()");
        }

        if (::close(pfd[1]) < 0) {
            throw std::runtime_error("Could not close()");
        }

        f(human);

        if (::close(1) < 0) {
            throw std::runtime_error("Could not close()");
        }

        ::wait(NULL);
    }
}

#else

template <typename FUNC>
void humanistic_output(FUNC f) {
    f(false);
}

#endif
